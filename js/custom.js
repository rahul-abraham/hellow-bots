// Nav
window.addEventListener("scroll", function () {
  var nav = document.querySelector(".navbar");

  if (window.scrollY >= 50) {
    nav.classList.add("navbar--fixed");
    document.body.classList.add("scrolled");
  } else {
    nav.classList.remove("navbar--fixed");
    document.body.classList.remove("scrolled");
  }
});


// Gallery
$(".gallery").magnificPopup({
  delegate: "a", // child items selector, by clicking on it popup will open
  type: "image",
  gallery: {
    enabled: true,
  },
});

// Video Initialise
$(".youtube-link").grtyoutube({
  autoPlay: true,
});

$(window).on('load',function(){
  var delayMs = 8000; // delay in milliseconds
  
  setTimeout(function(){
      $('#register').modal('show');
  }, delayMs);
});